package rm120010.parsing_help;
import rm120010.parsing_help.ParsingObjects.ConditionFact;
import rm120010.parsing_help.ParsingObjects.FunctionArguments;
import rm120010.parsing_help.ParsingObjects.IfConstruct;
import rm120010.parsing_help.ParsingObjects.OrConditions;
import rm120010.parsing_help.ParsingObjects.WhileConstruct;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.mj.runtime.Run;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Scope;
import rs.etf.pp1.symboltable.concepts.Struct;

public class ParsingHelper {
	
	public ParsingState myState = new ParsingState();
	public ParsingObjects myObjects = new ParsingObjects();
	public ParsingUsage myUsage = new ParsingUsage();
    public ParsingGenerator myGenerator = new ParsingGenerator();
    public boolean hasError = false;
    
    public static final int String_ = 5;
    public static final int Bool_ = 6;
    
    public static final Struct stringType = new Struct(String_, Tab.charType);
    public static final Struct boolType = new Struct(Bool_);
    
    public static final String boolName = "bool";
    public static final String stringName = "string";

	public void newArrayParameter(Struct t, String id, int idleft) {
		Struct newStruct = new Struct(Struct.Array, t);
        newParameter(newStruct, id, idleft);
		
	}

	public void newParameter(Struct t, String id, int idleft) {
		myState.formalParameter();
		if ((myState.location & ParsingState.INSIDE_VIRTUAL)!=0 && myObjects.currentParam == myObjects.countParam)
		{
			error("Previse argumenata");
			return;
		}
		if ((myState.location & ParsingState.INSIDE_VIRTUAL)!=0)
		{
			Struct paramBase = getParameterType(myObjects.currentBaseFunction, myObjects.currentParam);
			if (!compatibleTypes(paramBase, t))
			{
				error("Pogresan tip argumenta " + id);
			}
		}
		Obj param = Tab.insert(Obj.Var, id, t);
		param.setFpPos(myGenerator.fpPos++);
		param.setAdr(myGenerator.localDataPtr++);
		if (myObjects.currentMethod != null)
		  myObjects.currentMethod.setLevel(myObjects.currentMethod.getLevel() + 1);
		if ((myState.location & ParsingState.INSIDE_VIRTUAL)!=0)
			++myObjects.currentParam;
	}

	public void newLocalList() {
		// TODO Auto-generated method stub
		
	}

	public void newFormalList() {
		// TODO Auto-generated method stub
		
	}
	
	public boolean compatibleTypes(Struct type1, Struct type2)
	{
		Struct top = type2;
		while (top != null)
		{
		    top = myObjects.classHierarchy.get(top);
		    if (top != null && top.assignableTo(type1))
		    	return true;
		}
		return type2.assignableTo(type1);
	}

	public void newFunctionType(Struct t, String id, int line) {
		myState.enterFunction(id.equals("main"));
		if ("main".equals(id) && t != null)
		{
			error("Main mora biti void");
		}
		myObjects.currentMethod = null;
		myObjects.currentParam = 0;
		myObjects.countParam = 0;
		myObjects.hasReturn = false;
		Obj oldMethod = Tab.currentScope().findSymbol(id);
		if (t == null)
		  t = Tab.noType;
		if ((myState.location & ParsingState.INSIDE_CLASS) == 0)
		{
	         if (oldMethod != null)
	         {
	        	 error("Vec postoji takva metoda", line);
	         }
	         else
	         {
		         myObjects.currentMethod = Tab.insert(Obj.Meth, id, t);
		         if ("main".equals(id))
		         {
		        	 myObjects.mainFunction = myObjects.currentMethod;
		        	 for (Obj obj : myObjects.currentProgramScope.getLocals().symbols())
		        	 {
		        		 if (obj.getKind() == Obj.Type && obj.getType().getKind() == Struct.Class)
		        			 myGenerator.makeVTable(obj.getType());
		        	 }
		        	 myGenerator.jumpStrings();
		         }
	         }
		}
		else
		{
			if (oldMethod != null)
			{
			    if (oldMethod.getKind() != Obj.Meth)
			    {
			    	error("Vec postoji takvo polje", line);
			    }
			    else
			    {
			    	if (myObjects.extendsClassStruct == null)
			    		myObjects.currentBaseFunction = null;
			    	else myObjects.currentBaseFunction = myObjects.extendsClassStruct.getMembers().searchKey(id);
			    	if (myObjects.currentBaseFunction == null)
			    	{
			    		error("Vec postoji takva funkcija u tekucoj klasi", line);
			    	}
			    	else if (!compatibleTypes(myObjects.currentBaseFunction.getType(), t))
			    	{
			    		error("Pogresan povatni tip virtuelne metode", line);
			    	}
			    	else
			    	{
			    		myState.enterVirtual();
			    		myObjects.currentMethod = oldMethod;
			    		myObjects.countParam = myObjects.currentBaseFunction.getLevel() + 1;
			    		myObjects.currentParam = 0;
			    		oldMethod.getLocalSymbols().clear();
			    	}
			    }
			}
			else
			{
				myObjects.currentMethod = Tab.insert(Obj.Meth, id, t);
			}
		}
		if (myObjects.currentMethod != null)
			myObjects.currentMethod.setLevel(0);
		myGenerator.startArgs(isGlobal(myObjects.currentMethod));
		Tab.openScope();
		if ((myState.location & ParsingState.INSIDE_CLASS) != 0)
		{
			Obj obj = Tab.insert(Obj.Var, "this", myObjects.currentClassStruct);
			//change also enterFunction
			obj.setAdr(0);
			obj.setFpPos(-1);
		}
	}

	public void newVarMember(String id, int line) {
		if (Tab.currentScope().findSymbol(id) != null)
		{
			error("Vec postoji ta promenljiva " + id, line);
			return;
		}
		myState.varFound();
		int kind;
		if ((myState.location & ParsingState.INSIDE_CLASS) == 0 || (myState.location & ParsingState.INSIDE_FUNCTION) != 0)
			kind = Obj.Var;
		else
			kind = Obj.Fld;
		Obj var;
		if (myObjects.extendsFields == null || myObjects.extendsFields.findSymbol(id) == null)
		{
		var = Tab.insert(kind, id, myObjects.currentMemberType);
		myGenerator.newVar(var, isGlobal(var));
		}
		else
		{
			Obj old = myObjects.extendsFields.findSymbol(id);
			var = Tab.insert(kind, id, myObjects.currentMemberType);
			var.setAdr(old.getAdr());
		}
		if ((myState.location & ParsingState.INSIDE_FUNCTION) != 0)
			var.setFpPos(-1);
		
	}

	public void newVarType(Struct t) {
		myObjects.currentMemberType = t;
		
	}

	public void newVarArrayMember(String id, int idleft) {
		myState.varArrayFound();
		if (Tab.currentScope().findSymbol(id) != null)
		{
			error("Vec postoji ta promenljiva " + id, idleft);
			return;
		}
		Struct newStruct = new Struct(Struct.Array, myObjects.currentMemberType);
		int kind;
		if ((myState.location & ParsingState.INSIDE_CLASS) == 0 || (myState.location & ParsingState.INSIDE_FUNCTION) != 0)
			kind = Obj.Var;
		else
			kind = Obj.Fld;
		Obj var;
		if (myObjects.extendsFields == null || myObjects.extendsFields.findSymbol(id) == null)
		{
		var = Tab.insert(kind, id, newStruct);
		myGenerator.newVar(var, isGlobal(var));
		}
		else
		{
			Obj old = myObjects.extendsFields.findSymbol(id);
			var = Tab.insert(kind, id, newStruct);
			var.setAdr(old.getAdr());
		}
		if ((myState.location & ParsingState.INSIDE_FUNCTION) != 0)
			var.setFpPos(-1);
		
	}

	public int getConstValue(Object val)
	{
		if (val instanceof Integer)
		{
			return (Integer)val;
		}
		else if (val instanceof Character)
		{
			return (int)((Character)(val));
		}
		else if (val instanceof Boolean)
		{
			return ((Boolean)val)?1:0;
		}
		else
		{
			return myGenerator.newConstString((String)val);
		}
	}
	public void newConstMember(String id, Object val, int idleft) {
		myState.constFound();
		if (Tab.currentScope().findSymbol(id) != null)
		{
			error("Vec postoji ta konstanta " + id, idleft);
			return;
		}
		Tab.insert(Obj.Con, id, myObjects.currentMemberType).setAdr(getConstValue(val));
	}

	public void newConstType(Struct t) {
		myObjects.currentMemberType = t;
		
	}
	
	public Struct newOperation(Struct t, Struct t1, int operation, int opleft)
	{
		Struct toRet;
		if (t == Tab.intType && t1 == Tab.intType)
			toRet = Tab.intType;
		/*
		else if (t == Tab.charType && t1 == Tab.charType)
			toRet = Tab.charType;*/
		else if (t == ParsingHelper.stringType && t1 == ParsingHelper.stringType && operation == ParsingGenerator.ADD_OPERATION)
			toRet = ParsingHelper.stringType;
		else
			toRet = Tab.noType;
		if (toRet != Tab.noType)
		   myGenerator.generateOperation(operation, toRet);
		else
		   error("Nedozvoljena aritmeticka operacija", opleft);
		return toRet;
	}

	public void newProgram(String id) {
		
		Tab.insert(Obj.Type, boolName, boolType);
		Tab.insert(Obj.Type, stringName, stringType);
		
		myGenerator.precompile();
	
		myObjects.universe = Tab.currentScope();
		
		myObjects.currentProgram = Tab.insert(Obj.Prog, id, Tab.noType);
		Tab.openScope();
		myObjects.currentProgramScope = Tab.currentScope();
		
		
	}

	public void newClass(String id) {
		myState.classEnter();
        myObjects.currentClassStruct = new Struct(Struct.Class);
        myObjects.currentClassObj = Tab.insert(Obj.Type, id, myObjects.currentClassStruct);
        Tab.openScope();
        myGenerator.newClass(myObjects.currentClassObj);
        myObjects.extendsClassStruct = null;
        myObjects.extendsFields = null;
	}

	public void newExtends(Struct t) {
		if (t != Tab.noType)
		{
			myObjects.classHierarchy.put(myObjects.currentClassStruct, t);
			myObjects.extendsFields = new Scope(null);
			for (Obj member : t.getMembers().symbols())
			{
				Obj obj;
				if (member.getKind() == Obj.Meth) 
				{
				    obj = Tab.insert(member.getKind(), member.getName(), member.getType());
					obj.setAdr(member.getAdr());
					Scope tempScope = new Scope(null);
					for (Obj part : member.getLocalSymbols())
					  tempScope.addToLocals(part);
					obj.setLocals(tempScope.getLocals());
				}
				else
				{
                    obj = (new Obj(member.getKind(), member.getName(), member.getType()));
                    myObjects.extendsFields.addToLocals(obj);
					myGenerator.newVar(obj, false);
					obj.setAdr(member.getAdr());
				}
				obj.setLevel(member.getLevel());
			}
		}
		myObjects.extendsClassStruct = t;
	}

	public void newBlock() {
		myState.blockFound();
		
	}

	public void newElse() {

		IfConstruct ifStatement = myObjects.ifStatements.peek();
		myGenerator.newElse(ifStatement);
		
	}

	public void newIf() {
		myObjects.ifStatements.push(new IfConstruct());
		myObjects.conditionReason = myObjects.ifStatements.peek();
        myObjects.conditionReason.orCond.add(new OrConditions());
        myObjects.condAddress = Code.pc;
		
	}

	public void newWhile() {
		myObjects.whileStatements.push(new WhileConstruct());
		myObjects.conditionReason = myObjects.whileStatements.peek();
		myGenerator.newWhile(myObjects.whileStatements.peek());
		myObjects.conditionReason.orCond.add(new OrConditions());
		myObjects.condAddress = Code.pc;
		
	}

	public void newBreak(int line) {
		if (myObjects.whileStatements.size() == 0)
			error("Break mora se nalaziti u while petlji", line);
		else
		    myGenerator.newBreak(myObjects.whileStatements.peek());
		
	}

	public void newDecrement(Obj o, int line) {
		if (o == Tab.noObj)
		{
			error("Pogresno dekrementiranje", line);
			return;
		}
	    if (o.getKind() == Obj.Var)
	    	myUsage.varUsed(o, line, isGlobal(o));
	    else if (o.getKind() == Obj.Fld)
	    	myUsage.fieldUsed(o, line);
	    else if (!(o.getKind() == Obj.Elem))
	    {
	    	error("Pogresno dekrementiranje", line);
	    	return;
	    }
	    myGenerator.singleOperandOperation(o, ParsingGenerator.DEC_OPERATION);
		
	}

	public void endArguments(boolean toRemove, int lleft) {
		
		if (myObjects.functionArguments.peek().currentParam != myObjects.functionArguments.peek().countParam && myObjects.functionArguments.peek().function != Tab.noObj)
		{
			error("Pogresan broj argumenata", lleft);
		}
		else if (myObjects.functionArguments.peek().function != Tab.noObj)
		{
			myGenerator.functionCall(myObjects.functionArguments.peek().function, isGlobal(myObjects.functionArguments.peek().function));
		}
		if (toRemove && myObjects.functionArguments.peek().function.getType() != Tab.noType)
			myGenerator.removeFromStack();
		myObjects.functionArguments.pop();
	}

	public void newFunctionCall(Obj o, int line) {
		myState.functionCalled();
		int currentParam;
		int paramCnt = o.getLevel();
        currentParam = 0;
		if (o.getKind() == Obj.Meth)
		{
			myUsage.functionCalled(o, line);
			myObjects.functionArguments.push(new FunctionArguments(paramCnt, o, currentParam));
			if (!isGlobal(o))
			  myGenerator.endDesignator();
		}
		else
		{
			error("Nije funkcija", line);
			myObjects.functionArguments.push(new FunctionArguments(0, Tab.noObj, 0));
			
		}
		
		
	}

	
	public boolean isGlobal(Obj o)
	{
		if (o == null)
			return true;
		for (Obj ob : myObjects.universe.getLocals().symbols())
		{
			if (ob == o)
				return true;
		}
		for (Obj ob : myObjects.currentProgramScope.getLocals().symbols())
		{
			if (ob == o)
				return true;
		}
		/*
		if (myObjects.universe.getLocals().searchKey(o.getName()) != null)
			return true;
	    if (myObjects.currentProgramScope.getLocals().searchKey(o.getName()) != null)
	    	return true;
	    	*/
	    return false;
	}
	
	public void newAssignment(Obj o, Struct t, int line) {
	    if (o.getKind() == Obj.Var)
	    	myUsage.varUsed(o, line, isGlobal(o));
	    else if (o.getKind() == Obj.Fld)
	    	myUsage.fieldUsed(o, line);
	    else if (!(o.getKind() == Obj.Elem))
	    {
	    	error("Leva strana mora biti polje, promenljiva ili element niza", line);
	    	return;
	    }
	    if (o.getName().equals("d"))
	    {
	    	int z;
	    	z= 0;
	    }
	    if (compatibleTypes(o.getType(), t))
	       myGenerator.generateAssignment(o);
	    else
	       error("Moraju biti kompatibilni tipovi", line);
		
	}

	public void newReturn(Struct t, int line) {
		if (myObjects.currentMethod == null)
			return;
		if (myObjects.currentMethod.getType() == Tab.noType && t != Tab.noType)
		{
			error("Metoda je void", line);
			return;
		}
		if (!compatibleTypes(t, myObjects.currentMethod.getType()) && t != Tab.noType)
		{
			error("Pogresan povratni ", line);
			myObjects.hasReturn = true;
		}
		else
		{
		    myGenerator.endFunction(t, true);
		    myObjects.hasReturn = true;
		}
		
	}

	public void newIncrement(Obj o, int line) {
		if (o == Tab.noObj)
		{
			error("Pogresno inkrementiranje", line);
			return;
		}
	    if (o.getKind() == Obj.Var)
	    	myUsage.varUsed(o, line, isGlobal(o));
	    else if (o.getKind() == Obj.Fld)
	    	myUsage.fieldUsed(o, line);
	    else if (!(o.getKind() == Obj.Elem))
	    {
	    	error("bla");
	    	return;
	    }
	    myGenerator.singleOperandOperation(o, ParsingGenerator.INC_OPERATION);
		
	}

	public void endFunction() {
		myState.exitFunction();
		
		if (myObjects.currentMethod != null)
		{
		    Tab.chainLocalSymbols(myObjects.currentMethod);
		    myGenerator.endFunction(myObjects.currentMethod.getType(), false);
			if (myObjects.currentMethod.getName().equals("main") && myObjects.currentParam > 0)
			{
				error("Main ne sme imati argumente");
			}
			if (myObjects.currentMethod.getType() != Tab.noType && !myObjects.hasReturn)
				error("Nema return iskaza u metodi " + myObjects.currentMethod.getName());
		}
		
			Tab.closeScope();
		
		myObjects.currentMethod = null;
		
	}

	public void endClass() {
		myState.classExit();
		Tab.chainLocalSymbols(myObjects.currentClassStruct);
		Tab.closeScope();
		myObjects.extendsFields = null;
		myObjects.currentClassObj = null;
		myObjects.extendsClassStruct = null;
		myObjects.currentClassStruct = null;
		
	}
	
	public void printOutCounts()
	{
		myState.counter.dump();
	}

	public void classVarEnd() {
		if (myObjects.extendsFields != null)
		{
			for (Obj o : myObjects.extendsFields.getLocals().symbols())
			{
				Obj obj = Tab.insert(o.getKind(), o.getName(), o.getType());
				obj.setAdr(o.getAdr());
			}
		}
		Tab.chainLocalSymbols(myObjects.currentClassStruct);
	}

	public void programEnd() {
		Tab.chainLocalSymbols(myObjects.currentProgram);
		Tab.closeScope();
		if (myObjects.mainFunction != null)
		{
			myGenerator.startHere(myObjects.mainFunction);
		}
		else
		{
			error("Nema main funkcije");
		}
		if (!hasError) 
	      myGenerator.dump();
		
	}

	public Struct getType(String id, int idleft) {
        Obj obj = Tab.find(id);
        if (obj == Tab.noObj || obj.getKind() != Obj.Type)
        {
    		error("Nepostojeci tip " + id, idleft);
        }
        return obj.getType();
	}

	public Obj findObj(String id1) {
		if (id1.contains("[]"))
		{
			String id2 = id1.substring(0, id1.lastIndexOf("[]"));
	        Obj base = Tab.find(id2);
	        return new Obj(Obj.Elem, "", base.getType().getElemType());
		}
		else
	    {
			Obj base = Tab.find(id1);
			if (base.getKind() == Obj.Fld)
				myGenerator.thisParamLoad();
			return base;
	    }
	}

	public Obj findObj(Obj o1, String id2) {
		if (o1 == Tab.noObj) return Tab.noObj;
		if (o1.getType().getKind() != Struct.Class) return Tab.noObj;
		boolean isArray = false;
		if (id2.contains("[]"))
		{
			isArray = true;
			id2 = id2.substring(0, id2.lastIndexOf("[]"));
		}
        for (Obj toRet : o1.getType().getMembers().symbols())
        {
        	if (toRet.getName().equals(id2))
        	{
        		if (!isArray)
        			return toRet;
        		else
        		{
        			return new Obj(Obj.Elem, "", toRet.getType().getElemType());
        		}
        	}
        }
        return Tab.noObj;
	}

	public void newFactor(Obj o, int line) {
		if (o == Tab.noObj)
		{
		    error("Nepostojeci objekat", line);
		    return;
		}
	    if (o.getKind() == Obj.Con)
	    	myUsage.constUsed(o, line);
	    else if (o.getKind() == Obj.Var)
	    	myUsage.varUsed(o, line, isGlobal(o));
	    else if (o.getKind() == Obj.Fld)
	    	myUsage.fieldUsed(o, line);
	    if (o.getKind() == Obj.Con)
	      myGenerator.insertConst(o);
	    else 
	      myGenerator.loadThis(o);
		
	}
	
	public Obj structToObj(Struct t)
	{
		for (Obj o : myObjects.currentProgramScope.getLocals().symbols())
		{
			if (o.getKind() == Obj.Type && o.getType() == t)
				return o;
		}
		return Tab.noObj;
	}
	
	public void objectCreate(Struct t, int line) {
		if (t.getKind() == Struct.Class)
		{
			myUsage.objectCreate(structToObj(t),line);
			myGenerator.newStatement(t);
		}
		
	}

	public void newRead(Obj o, int line) {
		if ((o.getKind() == Obj.Fld || o.getKind() == Obj.Var || o.getKind() == Obj.Elem) && o != Tab.noObj)
		{
			myGenerator.newRead(o);
		}
		else
		{
			error("Nevalidno citanje", line);
		}
		
	}
	
	public void error(String reason, int line)
	{
		hasError = true;
		myGenerator.hasError = true;
		System.err.println(reason + " " + line);
		System.err.flush();
	}
	
	private void error(String reason) {
		hasError = true;
		myGenerator.hasError = true;
		System.err.println(reason);
		System.err.flush();
	}
	
	public Struct getParameterType(Obj function, int index)
	{
		for (Obj var : function.getLocalSymbols())
		{
			if (var.getFpPos() == index)
			{
				return var.getType();
			}
		}
		return Tab.noType;
	}
	
	public void newArgument(Struct e, int eleft) {
		FunctionArguments args = myObjects.functionArguments.peek();
		if (args.countParam <= args.currentParam)
		{
			++args.currentParam;
			return;
		}
		Struct base = getParameterType(args.function, args.currentParam);
		if (!compatibleTypes(base, e))
		{
			error("Pogresan tip argumenta", eleft);
		}
		++args.currentParam;
	}

	public void newPrint(Struct t, Integer width, int line) {
		if (t != Tab.charType && t != Tab.intType && t != ParsingHelper.stringType)
			error("Ne moze se stampati", line);
		else
		    myGenerator.newPrint(t, width);
		
		
	}

	public Struct checkInteger(Struct t, int tleft) {
		if (!(t == Tab.intType) && t != Tab.noType)
		{
			error("Mora biti integer ", tleft);
			return Tab.noType;
		}
		return t;
	}

	public Struct checkIntegers(Struct t, int tleft, Struct t1, int t1left) {
        Struct param1 = checkInteger(t, tleft);
        Struct param2 = checkInteger(t1, t1left);
        if (param1 == Tab.noType || param2 == Tab.noType)
        	return Tab.noType;
        return Tab.intType;
	}

	public void whileEnd() {
		
		myGenerator.endWhile(myObjects.whileStatements.peek());
		myObjects.whileStatements.pop();
		
	}

	public void enterFunction() {
		if (myObjects.currentMethod != null)
		  myGenerator.enterFunction(myObjects.currentMethod, isGlobal(myObjects.currentMethod));
		
	}

	public void newNegate() {
		myGenerator.singleOperandOperation(null, ParsingGenerator.NEG_OPERATION);
		
	}

	public void endIf() {
		myGenerator.fixIf(myObjects.ifStatements.peek());
		myObjects.ifStatements.pop();
		
	}

	public void newCondFact(Struct e1, Struct e2, Integer op, int line) {
		if ((e1 == Tab.intType && e2 == Tab.intType) || (e1 == Tab.charType && e2 == Tab.charType))
			myGenerator.newCondFact(myObjects.conditionReason, op, myObjects.condAddress);
		else if (e1 == ParsingHelper.stringType && e2 == ParsingHelper.stringType && op == ParsingGenerator.CMP_EQUAL)
			myGenerator.newCondFactString(myObjects.conditionReason, myObjects.condAddress);
		else
			error("Nedozvoljeno poredjenje", line);
		
	}

	public void noMoreCondition() {
		try{
		OrConditions orFact = myObjects.conditionReason.orCond.get(myObjects.conditionReason.orCond.size() - 1);
		ConditionFact andFact = orFact.andCond.get(orFact.andCond.size() - 1);
		myGenerator.nextIsNothing(myObjects.conditionReason, andFact);}
		catch(Exception e) {}
		
	}

	public void andIsNext() {
		try{
		OrConditions orFact = myObjects.conditionReason.orCond.get(myObjects.conditionReason.orCond.size() - 1);
		ConditionFact andFact = orFact.andCond.get(orFact.andCond.size() - 1);
		myObjects.condAddress = myGenerator.nextIsAnd(andFact);}
		catch(Exception e) {}
		
	}

	public void orIsNext() {
		try{
		OrConditions orFact = myObjects.conditionReason.orCond.get(myObjects.conditionReason.orCond.size() - 1);
		ConditionFact andFact = orFact.andCond.get(orFact.andCond.size() - 1);
		myObjects.condAddress = myGenerator.nextIsOr(myObjects.conditionReason, andFact);
		myObjects.conditionReason.orCond.add(new OrConditions());}
		catch(Exception e) {}
	}

	public void newComplexObj(Obj o1) {
		myGenerator.loadThis(o1);
		myObjects.currentBase = o1;
		
	}

	public void newSimpleObject(Object simple) {
		myGenerator.generateSimple(simple);
		
	}

	public Struct objectArrayCreate(Struct t, int tleft) {
		Struct toRet = new Struct(Struct.Array, t);
		myGenerator.newArrayStatement(t);
		return toRet;
		
	}

	public String getElem(String id) {
        return id + "[]";
	}

	public void newArrayMember(String id) {
		if (myObjects.currentBase != null)
		{
			Obj obj = findObj(myObjects.currentBase, id);
			myGenerator.loadThis(obj);
		}
		else
		{
			Obj obj = findObj(id);
			if (obj.getKind() == Obj.Fld)
				myGenerator.thisParamLoad();
			myGenerator.loadThis(obj);
		}
		
	}

	public void startDesignator() {
		myObjects.currentBase = null;
		myGenerator.startDesignator();
	}

	public void newCondFact(Struct e, int eleft) {
		if (e != boolType)
		{
			error("Mora biti boolean", eleft);
		}
		else
		{
			myGenerator.newBoolCondition(myObjects.conditionReason, myObjects.condAddress);
		}
		
	}

	public void newString(String s) {
		myGenerator.newString(s);
		
	}

	public void syntaxError() {
		hasError = true;
		myGenerator.hasError = true;
		
	}

	public void arrayIndex(Struct t1, int line) {
		if (t1 != Tab.intType)
		{
			error("Indeks mora biti integer", line);
		}
		
	}

}