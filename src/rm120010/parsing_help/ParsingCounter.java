package rm120010.parsing_help;

public class ParsingCounter {

	public int globalVar = 0;
	public int localMainVar = 0;
	public int globalConst = 0;
	public int globalArrayVar = 0;
	public int globalFunctionDefinition = 0;
	public int blocks = 0;
	public int localMainFunctionCalls = 0;
	public int formalArgumentDeclaration = 0;
	public int classDeclaration = 0;
	public int classFunctionDefinition = 0;
	public int classFieldDefinition = 0;

	public void newFunctionCall(int location) {
		if ((location & ParsingState.INSIDE_MAIN) != 0) 
			++localMainFunctionCalls;
		
	}

	public void newClass(int location) {
		++classDeclaration;
	}

	public void newBlock(int location) {
		++blocks;
		// TODO Auto-generated method stub
		
	}

	public void newFunctionDeclaration(int location) {
		if ((location & ParsingState.INSIDE_CLASS) != 0)
			++classFunctionDefinition;
		else
	        ++globalFunctionDefinition;
		// TODO Auto-generated method stub
		
	}

	public void newVar(int location) {
		if ((location & ParsingState.INSIDE_CLASS) != 0)
			++classFieldDefinition;
		else if ((location & ParsingState.INSIDE_FUNCTION) == 0)
	        ++globalVar;
		else if ((location & ParsingState.INSIDE_MAIN) != 0)
			++localMainVar;
		// TODO Auto-generated method stub
		
	}

	public void newConst(int location) {
		++globalConst;
		// TODO Auto-generated method stub
		
	}

	public void newArrayVar(int location) {
		if ((location & ParsingState.INSIDE_CLASS) != 0)
			++classFieldDefinition;
		else if ((location & ParsingState.INSIDE_FUNCTION) == 0)
	        ++globalArrayVar;
		else if ((location & ParsingState.INSIDE_MAIN) != 0)
			++localMainVar;
		// TODO Auto-generated method stub
		
	}

	public void newFormalParameter(int location) {
		++formalArgumentDeclaration;
		
	}
	
	public void dump()
	{
		System.out.println("Global vars: " + globalVar);
		System.out.println("Main local vars: " + localMainVar);
		System.out.println("Global consts: " + globalConst);
		System.out.println("Global arrays: " + globalArrayVar);
		System.out.println("Program function definitions " + globalFunctionDefinition);
		System.out.println("Blocks: " + blocks);
		System.out.println("Main function calls: " + localMainFunctionCalls);
		System.out.println("Formal aguments: " + formalArgumentDeclaration);
		System.out.println("Class definitions: " + classDeclaration);
		System.out.println("Class method definitions: " + classFunctionDefinition);
		System.out.println("Class field definitions: " + classFieldDefinition);
	}

}
