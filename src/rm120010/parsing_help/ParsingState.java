package rm120010.parsing_help;

import rs.etf.pp1.symboltable.Tab;

public class ParsingState {
    
	public static final int INSIDE_CLASS = 1;
	public static final int INSIDE_FUNCTION = 2;
	public static final int INSIDE_MAIN = 4;
	public static final int INSIDE_VIRTUAL = 8;
	
	public int location = 0;

	public ParsingCounter counter = new ParsingCounter();
    
	public void formalParameter()
	{
		counter.newFormalParameter(location);
	}

	public void functionCalled() {
		counter.newFunctionCall(location);
		// TODO Auto-generated method stub
		
	}

	public void blockFound() {
		counter.newBlock(location);
		
	}

	public void exitFunction() {
		location &= ~INSIDE_FUNCTION;
		location &= ~INSIDE_MAIN;
		location &= ~INSIDE_VIRTUAL;
		// TODO Auto-generated method stub
		
	}

	public void enterFunction(boolean isMain) {
		location |= INSIDE_FUNCTION;
		if (isMain) location |= INSIDE_MAIN;
		counter.newFunctionDeclaration(location);
		// TODO Auto-generated method stub
		
	}

	public void varFound() {
		counter.newVar(location);
		
	}

	public void constFound() {
		
		counter.newConst(location);
		// TODO Auto-generated method stub
		
	}

	public void classExit() {
		location &= ~INSIDE_CLASS;
		// TODO Auto-generated method stub
		
	}

	public void varArrayFound() {
		counter.newArrayVar(location);
		// TODO Auto-generated method stub
		
	}

	public void classEnter() {
		location |= INSIDE_CLASS;
		counter.newClass(location);
		// TODO Auto-generated method stub
		
	}

	public void enterVirtual() {
		location |= INSIDE_VIRTUAL;
		
	}

}
