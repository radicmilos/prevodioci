package rm120010.parsing_help;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import rm120010.parsing_help.ParsingObjects.IfConstruct;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Scope;
import rs.etf.pp1.symboltable.concepts.Struct;

public class ParsingObjects {
	
	public Struct currentClassStruct;
	public Struct extendsClassStruct;
	public Obj currentClassObj;
	public Obj currentMethod;
	public Obj currentProgram;
	public Struct currentMemberType;
	public Scope currentProgramScope;
	public Obj currentBaseFunction;
	public Obj mainFunction;
	public int countParam;
	public int currentParam;
	public Stack<FunctionArguments> functionArguments = new Stack<FunctionArguments> ();
	public Stack<WhileConstruct> whileStatements = new Stack<WhileConstruct> ();
	public Stack<IfConstruct> ifStatements = new Stack<IfConstruct> ();
	public ConditionReason conditionReason;
	public int condAddress;
	public Obj currentBase;
	public Scope universe;
	public Scope extendsFields;
	public boolean hasReturn;
	public Map<Struct, Struct> classHierarchy = new HashMap<Struct, Struct> ();

	public static class WhileConstruct extends ConditionReason
	{
		int addressStart;
		List<Integer> breaks = new LinkedList<Integer> ();
	}

	public static class FunctionArguments
	{
		public int currentParam;
		public int countParam;
		public Obj function;
		
		public FunctionArguments(int countParam, Obj function, int currentParam)
		{
			this.countParam = countParam;
			this.currentParam = currentParam;
			this.function = function;
		}
	}
	
	public static class IfConstruct extends ConditionReason
	{
		public boolean hasElse;
		public int fixElse;
	}
	
	public static class ConditionReason
	{
		List<OrConditions> orCond = new LinkedList<OrConditions>();
		public int start;
	}
	public static class OrConditions
	{
		List<ConditionFact> andCond = new LinkedList<ConditionFact>();
	}
	public static class ConditionFact 
	{
		public int startAddress;
        public int fixAddress;
		public int opcode;
	}
	
	public static class Designator
	{
		public Designator(int start, int end) {
			this.start = start;
			this.end = end;
		}
		public int start;
		public int end;
	}

}
