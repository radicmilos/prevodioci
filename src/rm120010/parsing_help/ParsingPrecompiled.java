package rm120010.parsing_help;

import java.util.Map;
import java.util.Map.Entry;

import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.concepts.Obj;

public class ParsingPrecompiled {
	
	ParsingGenerator parent;
	public ParsingPrecompiled(ParsingGenerator parent) {
		this.parent = parent;
	}
	public void makeLen(Obj lenMethod)
	{
		lenMethod.setAdr(Code.pc);
		parent.enterFunction(lenMethod, true);
		Code.put(Code.load); 
		Code.put(0);
		Code.put(Code.arraylength);
		parent.makeReturn();
	}
	public void makeCopyString(Obj lenMethod, Obj copyMethod)
	{
		copyMethod.setAdr(Code.pc);
		Code.put(Code.enter);
		Code.put(1);
		Code.put(4);
		Code.put(Code.load_n);
		Code.put(Code.call); 
		Code.put2(lenMethod.getAdr() - Code.pc + 1);
		Code.put(Code.store_2);
		Code.put(Code.load_2);
		Code.put(Code.newarray);
		Code.put(0);
		Code.put(Code.store_1);
		Code.put(Code.const_n);
		Code.put(Code.store_3);
		Code.put(Code.load_3);
		Code.put(Code.load_2);
		Code.put(Code.jcc + Code.ge);
		Code.put2(16);
		Code.put(Code.load_1);
		Code.put(Code.load_3);
		Code.put(Code.load_n);
		Code.put(Code.load_3);
		Code.put(Code.baload);
		Code.put(Code.bastore);
		Code.put(Code.load_3);
		Code.put(Code.const_1);
		Code.put(Code.add);
		Code.put(Code.store_3);
		Code.put(Code.jmp);
		Code.put2(-15);
		Code.put(Code.load_1);
		Code.put(Code.exit);
		Code.put(Code.return_);
	}
	public void makeStringMake(Obj stringMakeMethod, Map<String, Integer> programStrings) 
	{
		stringMakeMethod.setAdr(Code.pc);
		Code.put(Code.enter);
		Code.put(0);
		Code.put(0);
		for (Entry<String, Integer> entry : programStrings.entrySet())
		{
			Code.put(Code.const_);
			Code.put4(entry.getKey().length() + 1);
			Code.put(Code.newarray);
			Code.put(0);
			Code.put(Code.putstatic);
			Code.put2(entry.getValue());
			for (int i = 0; i < entry.getKey().length(); ++i)
			{
				Code.put(Code.getstatic);
				Code.put2(entry.getValue());
				Code.put(Code.const_);
				Code.put4(i);
				Code.put(Code.const_);
				Code.put4(entry.getKey().charAt(i));
				Code.put(Code.bastore);
			}
			Code.put(Code.getstatic);
			Code.put2(entry.getValue());
			Code.put(Code.const_);
			Code.put4(entry.getKey().length());
			Code.put(Code.const_);
			Code.put4(0);
			Code.put(Code.bastore);
		}
		parent.endFunction(stringMakeMethod.getType(), true);
		
	}
	
	public void makePrintString(Obj lenMethod, Obj printMethod)
	{
		printMethod.setAdr(Code.pc);
		Code.put(Code.enter);
		Code.put(2);
		Code.put(4);
		Code.put(Code.load_n);
		Code.put(Code.call);
		Code.put2(lenMethod.getAdr() - Code.pc + 1);
		Code.put(Code.store_2);
		Code.put(Code.const_1);
		Code.put(Code.store_3);
		Code.put(Code.load_n);
		Code.put(Code.const_n);
		Code.put(Code.baload);
		Code.put(Code.load_1);
		Code.put(Code.bprint);
		Code.put(Code.load_3);
		Code.put(Code.load_2);
		Code.put(Code.const_1);
		Code.put(Code.sub);
		Code.put(Code.jcc + Code.ge);
		Code.put2(15);
		Code.put(Code.load_n);
		Code.put(Code.load_3);
		Code.put(Code.baload);
		Code.put(Code.const_n);
		Code.put(Code.bprint);
		Code.put(Code.load_3);
		Code.put(Code.const_1);
		Code.put(Code.add);
		Code.put(Code.store_3);
		Code.put(Code.jmp);
		Code.put2(-16);
		Code.put(Code.exit);
		Code.put(Code.return_);
	}
	
	public void makeCompareString(Obj lenMethod, Obj compareMethod)
	{
		compareMethod.setAdr(Code.pc);
		Code.put(Code.enter);
		Code.put(2);
		Code.put(5);
		Code.put(Code.load_n);
		Code.put(Code.call);
		Code.put2(lenMethod.getAdr() - Code.pc + 1);
		Code.put(Code.store_2);
		Code.put(Code.load_1);
		Code.put(Code.call);
		Code.put2(lenMethod.getAdr() - Code.pc + 1);
		Code.put(Code.store_3);
		Code.put(Code.load_2);
		Code.put(Code.load_3);
		Code.put(Code.jcc + Code.eq);
		Code.put2(6);
		Code.put(Code.const_n);
		Code.put(Code.exit);
		Code.put(Code.return_);
		Code.put(Code.const_n);
		Code.put(Code.store);
		Code.put(4);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.load_2);
		Code.put(Code.jcc + Code.ge);
		Code.put2(26);
		Code.put(Code.load_n);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.baload);
		Code.put(Code.load_1);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.baload);
		Code.put(Code.jcc + Code.eq);
		Code.put2(6);
		Code.put(Code.const_n);
		Code.put(Code.exit);
		Code.put(Code.return_);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.const_1);
		Code.put(Code.add);
		Code.put(Code.store);
		Code.put(4);
		Code.put(Code.jmp);
		Code.put2(-26);
		Code.put(Code.const_1);
		Code.put(Code.exit);
		Code.put(Code.return_);
	}
	
	public void makeOrdMethod(Obj ordMethod)
	{
		ordMethod.setAdr(Code.pc);
		Code.put(Code.enter);
		Code.put(1);
		Code.put(1);
		Code.put(Code.load_n);
		Code.put(Code.exit);
		Code.put(Code.return_);
	}
	
	public void makeChrMethod(Obj chrMethod)
	{
		chrMethod.setAdr(Code.pc);
		Code.put(Code.enter);
		Code.put(1);
		Code.put(1);
		Code.put(Code.load_n);
		Code.put(Code.exit);
		Code.put(Code.return_);		
	}
	
	public void makeAddString(Obj lenMethod, Obj addStringMethod)
	{
		addStringMethod.setAdr(Code.pc);
		Code.put(Code.enter);
		Code.put(2);
		Code.put(6);
		Code.put(Code.load_n);
		Code.put(Code.call);
		Code.put2(lenMethod.getAdr() - Code.pc + 1);
		Code.put(Code.store_2);
		Code.put(Code.load_1);
		Code.put(Code.call);
		Code.put2(lenMethod.getAdr() - Code.pc + 1);
		Code.put(Code.store_3);
		Code.put(Code.load_2);
		Code.put(Code.load_3);
		Code.put(Code.add);
		Code.put(Code.const_1);
		Code.put(Code.sub);
		Code.put(Code.newarray);
		Code.put(0);
		Code.put(Code.store);
		Code.put(4);
		Code.put(Code.const_n);
		Code.put(Code.store);
		Code.put(5);
		Code.put(Code.load);
		Code.put(5);
		Code.put(Code.load_2);
		Code.put(Code.const_1);
		Code.put(Code.sub);
		Code.put(Code.jcc + Code.ge);
		Code.put2(21);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.load);
		Code.put(5);
		Code.put(Code.load_n);
		Code.put(Code.load);
		Code.put(5);
		Code.put(Code.baload);
		Code.put(Code.bastore);
		Code.put(Code.load);
		Code.put(5);
		Code.put(Code.const_1);
		Code.put(Code.add);
		Code.put(Code.store);
		Code.put(5);
		Code.put(Code.jmp);
		Code.put2(-23);
		Code.put(Code.const_n);
		Code.put(Code.store);
		Code.put(5);
		Code.put(Code.load);
		Code.put(5);
		Code.put(Code.load_3);
		Code.put(Code.const_1);
		Code.put(Code.sub);
		Code.put(Code.jcc + Code.ge);
		Code.put2(25);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.load);
		Code.put(5);
		Code.put(Code.load_2);
		Code.put(Code.const_1);
		Code.put(Code.sub);
		Code.put(Code.add);
		Code.put(Code.load_1);
		Code.put(Code.load);
		Code.put(5);
		Code.put(Code.baload);
		Code.put(Code.bastore);
		Code.put(Code.load);
		Code.put(5);
		Code.put(Code.const_1);
		Code.put(Code.add);
		Code.put(Code.store);
		Code.put(5);
		Code.put(Code.jmp);
		Code.put2(-27);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.load_2);
		Code.put(Code.load_3);
		Code.put(Code.const_2);
		Code.put(Code.sub);
		Code.put(Code.add);
		Code.put(Code.const_n);
		Code.put(Code.bastore);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.exit);
		Code.put(Code.return_);
	}
	
	public void makeReadString(Obj readStringMethod)
	{
		readStringMethod.setAdr(Code.pc);
		Code.put(Code.enter);
		Code.put(0);
		Code.put(9);
		Code.put(Code.const_1);
		Code.put(Code.newarray);
		Code.put(0);
		Code.put(Code.store_1);
		Code.put(Code.load_1);
		Code.put(Code.const_n);
		Code.put(Code.const_n);
		Code.put(Code.bastore);
		Code.put(Code.load_1);
		Code.put(Code.call);
		Code.put2(-236);
		Code.put(Code.store_n);
		Code.put(Code.const_);
		Code.put4(21);
		Code.put(Code.newarray);
		Code.put(0);
		Code.put(Code.store_1);
		Code.put(Code.const_);
		Code.put4(20);
		Code.put(Code.store);
		Code.put(6);
		Code.put(Code.const_n);
		Code.put(Code.store);
		Code.put(4);
		Code.put(Code.const_n);
		Code.put(Code.store);
		Code.put(5);
		Code.put(Code.const_1);
		Code.put(Code.const_1);
		Code.put(Code.jcc + Code.ne);
		Code.put2(90);
		Code.put(Code.bread);
		Code.put(Code.store);
		Code.put(8);
		Code.put(Code.load);
		Code.put(8);
		Code.put(Code.const_);
		Code.put4(13);
		Code.put(Code.jcc + Code.eq);
		Code.put2(13);
		Code.put(Code.load);
		Code.put(8);
		Code.put(Code.const_);
		Code.put4(10);
		Code.put(Code.jcc + Code.ne);
		Code.put2(6);
		Code.put(Code.jmp);
		Code.put2(64);
		Code.put(Code.load);
		Code.put(6);
		Code.put(Code.const_1);
		Code.put(Code.sub);
		Code.put(Code.store);
		Code.put(6);
		Code.put(Code.load_1);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.load);
		Code.put(8);
		Code.put(Code.bastore);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.const_1);
		Code.put(Code.add);
		Code.put(Code.store);
		Code.put(4);
		Code.put(Code.load);
		Code.put(6);
		Code.put(Code.const_n);
		Code.put(Code.jcc + Code.ne);
		Code.put2(37);
		Code.put(Code.load_1);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.const_n);
		Code.put(Code.bastore);
		Code.put(Code.load_n);
		Code.put(Code.load_1);
		Code.put(Code.call);
		Code.put2(-196);
		Code.put(Code.call);
		Code.put2(-326);
		Code.put(Code.store_n);
		Code.put(Code.const_);
		Code.put4(20);
		Code.put(Code.store);
		Code.put(6);
		Code.put(Code.const_n);
		Code.put(Code.store);
		Code.put(4);
		Code.put(Code.load);
		Code.put(5);
		Code.put(Code.const_);
		Code.put4(20);
		Code.put(Code.add);
		Code.put(Code.store);
		Code.put(5);
		Code.put(Code.jmp);
		Code.put2(-89);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.const_n);
		Code.put(Code.jcc + Code.le);
		Code.put2(83);
		Code.put(Code.load);
		Code.put(5);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.const_1);
		Code.put(Code.add);
		Code.put(Code.add);
		Code.put(Code.newarray);
		Code.put(0);
		Code.put(Code.store_2);
		Code.put(Code.load_n);
		Code.put(Code.store_3);
		Code.put(Code.const_n);
		Code.put(Code.store);
		Code.put(7);
		Code.put(Code.load);
		Code.put(7);
		Code.put(Code.load);
		Code.put(5);
		Code.put(Code.jcc + Code.ge);
		Code.put2(20);
		Code.put(Code.load_2);
		Code.put(Code.load);
		Code.put(7);
		Code.put(Code.load_3);
		Code.put(Code.load);
		Code.put(7);
		Code.put(Code.baload);
		Code.put(Code.bastore);
		Code.put(Code.load);
		Code.put(7);
		Code.put(Code.const_1);
		Code.put(Code.add);
		Code.put(Code.store);
		Code.put(7);
		Code.put(Code.jmp);
		Code.put2(-21);
		Code.put(Code.const_n);
		Code.put(Code.store);
		Code.put(7);
		Code.put(Code.load);
		Code.put(7);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.jcc + Code.ge);
		Code.put2(23);
		Code.put(Code.load_2);
		Code.put(Code.load);
		Code.put(5);
		Code.put(Code.load);
		Code.put(7);
		Code.put(Code.add);
		Code.put(Code.load_1);
		Code.put(Code.load);
		Code.put(7);
		Code.put(Code.baload);
		Code.put(Code.bastore);
		Code.put(Code.load);
		Code.put(7);
		Code.put(Code.const_1);
		Code.put(Code.add);
		Code.put(Code.store);
		Code.put(7);
		Code.put(Code.jmp);
		Code.put2(-24);
		Code.put(Code.load_2);
		Code.put(Code.load);
		Code.put(5);
		Code.put(Code.load);
		Code.put(4);
		Code.put(Code.add);
		Code.put(Code.const_n);
		Code.put(Code.bastore);
		Code.put(Code.load_2);
		Code.put(Code.exit);
		Code.put(Code.return_);
		Code.put(Code.load_n);
		Code.put(Code.exit);
		Code.put(Code.return_);
	}

}
