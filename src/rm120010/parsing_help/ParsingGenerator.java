package rm120010.parsing_help;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import rm120010.parsing_help.ParsingObjects.ConditionFact;
import rm120010.parsing_help.ParsingObjects.ConditionReason;
import rm120010.parsing_help.ParsingObjects.Designator;
import rm120010.parsing_help.ParsingObjects.IfConstruct;
import rm120010.parsing_help.ParsingObjects.OrConditions;
import rm120010.parsing_help.ParsingObjects.WhileConstruct;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class ParsingGenerator {
	
	public ParsingHelper myParent;
	public OutputStream output;
	public ParsingPrecompiled myPrecompiler = new ParsingPrecompiled(this);

	public static final int ADD_OPERATION = 0;
	public static final int SUB_OPERATION = 1;
	public static final int MUL_OPERATION = 2;
	public static final int DIV_OPERATION = 3;
	public static final int MOD_OPERATION = 4;
	public static final int INC_OPERATION = 5;
	public static final int DEC_OPERATION = 6;
	public static final int NEG_OPERATION = 7;

	public static final int opcodes[] = {Code.add, Code.sub, Code.mul, Code.div, Code.rem};

	public static final int CMP_EQUAL = 0;
	public static final int CMP_NEQUAL = 1;
	public static final int CMP_BLS = 2;
	public static final int CMP_BLE = 3;
	public static final int CMP_BGR = 4;
	public static final int CMP_BGE = 5;
	
	public static final int relopcodes[] = {Code.eq, Code.ne, Code.lt,
		                                    Code.le, Code.gt, Code.ge};
	
	public int globalDataPtr = 0;
	public int localDataPtr = 0;
	public int classDataPtr = 0;
	public int fpPos = 0;
	
	public int vTableStart = -1;
	
	public int designatorStart;
	public int designatorEnd;

	public Map<Struct, Integer> vTables = new HashMap<Struct, Integer> ();
	public Stack<Designator> designators = new Stack<Designator> ();
	
	public Obj lenMethod;
	public Obj copyMethod;
	public Obj stringMakeMethod;
    public Obj printMethod;
    public Obj compareMethod;
    public Obj addStringMethod;
    public Obj ordMethod;
    public Obj chrMethod;
    public Obj readMethod;

	public int fixJumpString;
	public Map<String, Integer> programStrings = new HashMap<String, Integer> ();
	
	public boolean hasError = false;

	public ParsingGenerator()
	{
		if (hasError) return;
		try {
			output = new FileOutputStream("src/tst/proba.obj");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void newRead(Obj o)
	{
		if (hasError) return;
		if (o.getType() == Tab.intType)
		{
			Code.put(Code.read);
			Code.store(o);
		}
		else if (o.getType() == Tab.charType)
		{
			Code.put(Code.bread);
			Code.store(o);
		}
		else if (o.getType() == ParsingHelper.stringType)
		{
			Code.put(Code.call);
			Code.put2(readMethod.getAdr() - Code.pc + 1);
			Code.store(o);
		}
	}

	public void dump() {
		if (hasError) return;
		Code.dataSize = globalDataPtr;
		stringMakeMethod = new Obj(Obj.Meth, "stringMake", Tab.noType);
		Code.fixup(fixJumpString);
		myPrecompiler.makeStringMake(stringMakeMethod, programStrings);
		Code.write(output);
		
	}

	public void newString(String s)
	{
		if (hasError) return;
		int ptrString;
        if (!programStrings.containsKey(s))
        {
        	programStrings.put(s, globalDataPtr);
        	ptrString = globalDataPtr;
        	++globalDataPtr;
        }
        else
        	ptrString = programStrings.get(s);
        Code.put(Code.getstatic);
        Code.put2(ptrString);
	}
	
	
	public int newConstString(String s)
	{
		if (hasError) return 0;
		int ptrString;
        if (!programStrings.containsKey(s))
        {
        	programStrings.put(s, globalDataPtr);
        	ptrString = globalDataPtr;
        	++globalDataPtr;
        }
        else
        	ptrString = programStrings.get(s);
        return ptrString;
	}

	public void startHere(Obj mainFunction) {
		if (hasError) return;
		if (vTableStart == -1)
		  Code.mainPc = fixJumpString - 1;
		else
	      Code.mainPc = vTableStart;
		
	}

	public void newPrint(Struct t, Integer width) {
		if (hasError) return;
		if (t == ParsingHelper.stringType)
		{
			Code.loadConst(width);
			Code.put(Code.call);
			Code.put2(printMethod.getAdr() - Code.pc + 1);
		}
		else if (t == Tab.intType)
		{
			Code.loadConst(width);
			Code.put(Code.print);
		}
		else if (t == Tab.charType)
		{
			Code.loadConst(width);
			Code.put(Code.bprint);
		}
		
	}
	
	public void generateOperation(int operation, Struct toRet)
	{
		if (hasError) return;
		if (toRet == Tab.intType)
          Code.put(opcodes[operation]);
		else
		{
			Code.put(Code.call);
			Code.put2(addStringMethod.getAdr() - Code.pc + 1);
		}
	}

	public void generateAssignment(Obj o) {
		if (hasError) return;
		if (o.getType() != ParsingHelper.stringType)
		  Code.store(o);
		else
		{
			Code.put(Code.call);
			Code.put2(copyMethod.getAdr() - Code.pc + 1);
			Code.store(o);
		}
	}

	public void startArgs(boolean isGlobal)
	{
		fpPos = 0;
		if (isGlobal)
			  localDataPtr = 0;
			else
			{
				localDataPtr = 1;	
			}
		if (hasError) return;
	}
	
	public void enterFunction(Obj method, boolean isGlobal)
	{
		if (hasError) return;
		method.setAdr(Code.pc);
		Code.put(Code.enter);
		if (isGlobal)
		  Code.put(method.getLevel());
		else
		  Code.put(method.getLevel() + 1);
		if (isGlobal)
		  Code.put(Tab.currentScope().getnVars());
		else
			Code.put(Tab.currentScope().getnVars());	
	}
	
	public void endFunction(Struct type, boolean isThere) {
		if (hasError) return;
		if(type == Tab.noType || isThere)
		{
		    Code.put(Code.exit);
		    Code.put(Code.return_);
		}
		else
		{
			 Code.put(Code.trap); 
			 Code.put(1); 
		}
	}

	public void makeReturn()
	{
		if (hasError) return;
	    Code.put(Code.exit);
	    Code.put(Code.return_);	
	}

	public void newWhile(WhileConstruct statement) {
		if (hasError) return;
		statement.addressStart = Code.pc;
		
	}
	
	public void endWhile(WhileConstruct statement)
	{
		if (hasError) return;
		Code.putJump(statement.addressStart);
		for (int breakSrc : statement.breaks)
		  Code.fixup(breakSrc);
		OrConditions orCond = statement.orCond.get(statement.orCond.size() - 1);
		try{
		for (int i = 0; i < orCond.andCond.size(); ++i)
		{
		ConditionFact fact = orCond.andCond.get(i);
		Code.fixup(fact.fixAddress);
		}
		}
		catch(java.lang.IndexOutOfBoundsException ex) {};
	}

	public int getClassSize(Struct t)
	{
		if (hasError) return 0;
		return (t.getNumberOfFields() + 1) * 4;
	}
	public void newBreak(WhileConstruct peek) {
		if (hasError) return;
		Integer addr = Code.pc + 1;
		Code.putJump(0);
		peek.breaks.add(addr);
	}

	public void newStatement(Struct t) {
		if (hasError) return;
		if (t.getKind() != Struct.Class)
		{
			Code.put(Code.new_);
			Code.put2(t.getNumberOfFields());
		}
		
		else
		{
			Code.put(Code.new_);
			Code.put2(getClassSize(t));
			Code.put(Code.dup);
			int vTable = vTables.get(t);
			Code.loadConst(vTable);
			Code.put(Code.putfield);
			Code.put2(0);
			
		}

	}

	public void singleOperandOperation(Obj o, int operation) {
		if (hasError) return;
		if (operation == INC_OPERATION)
		{
			if (o.getKind() == Obj.Fld)
			  Code.put(Code.dup);
			else if (o.getKind() == Obj.Elem)
			  Code.put(Code.dup2);
			Code.load(o);
			Code.loadConst(1);
			Code.put(Code.add);
			Code.store(o);
		}
		else if (operation == DEC_OPERATION)
		{
			if (o.getKind() == Obj.Fld)
		      Code.put(Code.dup);
			else if (o.getKind() == Obj.Elem)
			  Code.put(Code.dup2);
			Code.load(o);
			Code.loadConst(1);
			Code.put(Code.sub);
			Code.store(o);
		}
		else if (operation == NEG_OPERATION)
		{
			Code.put(Code.neg);
		}
		
	}

	public void newElse(IfConstruct ifStatement) {
		if (hasError) return;
		ifStatement.hasElse = true;
		ifStatement.fixElse = Code.pc + 1;
		Code.putJump(0);
		Code.put2(0);
		OrConditions orCond = ifStatement.orCond.get(ifStatement.orCond.size() - 1);
		try{
		for (int i = 0; i < orCond.andCond.size(); ++i)
		{
		ConditionFact fact = orCond.andCond.get(i);
		Code.fixup(fact.fixAddress);
		}
		}
		catch(java.lang.IndexOutOfBoundsException ex) {};
		
	}

	public void fixIf(IfConstruct ifStatement) {
		if (hasError) return;
		if (ifStatement.hasElse)
			Code.fixup(ifStatement.fixElse);
		else
		{
		OrConditions orCond = ifStatement.orCond.get(ifStatement.orCond.size() - 1);
		try{
		for (int i = 0; i < orCond.andCond.size(); ++i)
		{
		ConditionFact fact = orCond.andCond.get(i);
		Code.fixup(fact.fixAddress);
		}
		}
		catch(java.lang.IndexOutOfBoundsException ex) {};
		}
	}

	public void newCondFact(ConditionReason conditionReason, Integer op, int start) {
		if (hasError) return;
        ConditionFact fact = new ConditionFact();
        fact.startAddress = start;
        fact.opcode = op;
        List<ConditionFact> facts= conditionReason.orCond.get(conditionReason.orCond.size() - 1).andCond;
        facts.add(fact);
		
	}

	public int nextIsAnd(ConditionFact andFact) {
		
		if (hasError) return 0;
		
		andFact.fixAddress = Code.pc + 1;
		Code.putFalseJump(relopcodes[andFact.opcode], 0);
		
		return Code.pc;
		
	}

	public int nextIsOr(ConditionReason reason, ConditionFact andFact) {
		if (hasError) return 0;
		andFact.fixAddress = Code.pc + 1;
		Code.put(Code.jcc + andFact.opcode);
		Code.put2(0);
		OrConditions orCond = reason.orCond.get(reason.orCond.size() - 1);
		for (int i = 0; i < orCond.andCond.size() - 1; ++i)
			Code.fixup(orCond.andCond.get(i).fixAddress);
		return Code.pc;
		
	}

	public int nextIsNothing(ConditionReason reason, ConditionFact andFact) {
		if (hasError) return 0;
		andFact.fixAddress = Code.pc + 1;
		Code.putFalseJump(relopcodes[andFact.opcode], 0);
		reason.start = Code.pc;
		
		for (int i = 0; i < reason.orCond.size() - 1; ++i)
		{
			ConditionFact fact = reason.orCond.get(i).andCond.get(reason.orCond.get(i).andCond.size() - 1);
			Code.fixup(fact.fixAddress);
		}

		return Code.pc;
	}

	public void newClass(Obj newClass)
	{
		if (hasError) return;
		classDataPtr = 1;
	}

	public void newVar(Obj var, boolean isGlobal) {
		if (hasError) return;
		if (isGlobal)
		{
			var.setAdr(globalDataPtr);
			++globalDataPtr;
		}
		else if (var.getKind() == Obj.Fld)
		{
			var.setAdr(classDataPtr);
			++classDataPtr;
		}
		else
		{
			var.setAdr(localDataPtr);
			++localDataPtr;
		}
	}

	public void loadThis(Obj o1) {
		if (hasError) return;
		Code.load(o1);
		
	}
	public void generateSimple(Object simple) {
		if (hasError) return;
		if (simple instanceof Integer)
		{
			Code.loadConst((Integer) simple);
		}
		else if (simple instanceof Character)
		{
			Code.loadConst((Character)simple);
		}
		else if (simple instanceof Boolean)
		{
			Code.loadConst(((Boolean)(simple))?1:0);
		}
		
	}
	public void functionCall(Obj o, boolean isGlobal) {
		if (hasError) return;
		int pcOld = Code.pc;
		if (isGlobal)
		{
		    Code.put(Code.call);
		    Code.put2(o.getAdr() - pcOld);
		}
		else
		{
			copyDesingator();
			Code.put(Code.getfield);
			Code.put2(0);
			Code.put(Code.invokevirtual);
			for (int i = 0; i < o.getName().length(); ++i)
				Code.put4(o.getName().charAt(i));
			Code.put4(-1);
			
		}
		
	}
	public void newArrayStatement(Struct t) {
		if (hasError) return;
		if (t.getKind() == Struct.Char)
		{
			Code.put(Code.newarray);
			Code.put(0);
		}
		if (t.getKind() == Struct.Int)
		{
			Code.put(Code.newarray);
			Code.put(1);
		}
	}
	
	public void putString(String s)
	{
		if (hasError) return;
		for (int i = 0; i < s.length(); ++i)
		{
			int toPut = s.charAt(i);
			Code.loadConst(toPut);
			Code.put(Code.putstatic);
			Code.put2(globalDataPtr);
			++globalDataPtr;
		}
	}
	public void makeVTable(Struct currentClassStruct) {
		if (hasError) return;
		vTables.put(currentClassStruct, globalDataPtr);
		if (vTableStart == -1)
			vTableStart = Code.pc;
		for (Obj method : currentClassStruct.getMembers().symbols())
		{
			if (method.getKind() != Obj.Meth)
				continue;
			putString(method.getName());
			Code.loadConst(-1);
			Code.put(Code.putstatic);
			Code.put2(globalDataPtr);
			++globalDataPtr;
			Code.loadConst(method.getAdr());
			Code.put(Code.putstatic);
			Code.put2(globalDataPtr);
			++globalDataPtr;
		}
		Code.loadConst(-2);
		Code.put(Code.putstatic);
		Code.put2(globalDataPtr);
		++globalDataPtr;
	}
	public void startDesignator() {
		if (hasError) return;
		designatorStart = Code.pc;
		
	}
	public void endDesignator() {
		if (hasError) return;
		designatorEnd = Code.pc;
		designators.push(new Designator(designatorStart, designatorEnd));
		
	}
	
	public void copyDesingator()
	{
		if (hasError) return;
		Designator d = designators.pop();
		for (int i = d.start; i < d.end; ++i)
		{
			Code.put(Code.buf[i]);
		}
	}
	public void thisParamLoad() {
		if (hasError) return;
		Code.put(Code.load);
		Code.put(0);
		
	}
	public void newBoolCondition(ConditionReason conditionReason, int start) {
		if (hasError) return;
		Code.loadConst(0);
        ConditionFact fact = new ConditionFact();
        fact.startAddress = start;
        fact.opcode = CMP_BGR;
        List<ConditionFact> facts= conditionReason.orCond.get(conditionReason.orCond.size() - 1).andCond;
        facts.add(fact);
		
	}
	public void precompile() {
		if (hasError) return;
		lenMethod = Tab.find("len");
		myPrecompiler.makeLen(lenMethod);
		ordMethod = Tab.find("ord");
		myPrecompiler.makeOrdMethod(ordMethod);
		chrMethod = Tab.find("chr");
		myPrecompiler.makeChrMethod(chrMethod);
		copyMethod = new Obj(Obj.Meth, "copyMethod", Tab.noType);
		myPrecompiler.makeCopyString(lenMethod, copyMethod);
		printMethod = new Obj(Obj.Meth, "printMethod", Tab.noType);
		myPrecompiler.makePrintString(lenMethod, printMethod);
		compareMethod = new Obj(Obj.Meth, "compareMethod", Tab.noType);
		myPrecompiler.makeCompareString(lenMethod, compareMethod);
		addStringMethod = new Obj(Obj.Meth, "stringAddMethod", Tab.noType);
		myPrecompiler.makeAddString(lenMethod, addStringMethod);
		readMethod = new Obj(Obj.Meth, "readMethod", Tab.noType);
		myPrecompiler.makeReadString(readMethod);
		
	}
	public void jumpStrings() {
		if (hasError) return;
		fixJumpString = Code.pc + 1;
		Code.put(Code.call);
		Code.put2(0);
		
	}
	public void newCondFactString(ConditionReason conditionReason,
			int start) {
		if (hasError) return;
		Code.put(Code.call);
		Code.put2(compareMethod.getAdr() - Code.pc + 1);
		Code.loadConst(0);
        ConditionFact fact = new ConditionFact();
        fact.startAddress = start;
        fact.opcode = CMP_BGR;
        List<ConditionFact> facts= conditionReason.orCond.get(conditionReason.orCond.size() - 1).andCond;
        facts.add(fact);
		
	}
	public void insertConst(Obj o) {
		if (hasError) return;
		if (o.getType() == ParsingHelper.stringType)
		{
			Code.put(Code.getstatic);
			Code.put2(o.getAdr());
		}
		else
		{
			Code.put(Code.const_);
			Code.put4(o.getAdr());
		}
		
	}
	public void removeFromStack() {
		Code.put(Code.pop);
		
	}
	
	
}
