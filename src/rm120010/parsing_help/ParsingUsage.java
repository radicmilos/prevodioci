package rm120010.parsing_help;

import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class ParsingUsage {

	public void functionCalled(Obj function, int line)
	{
		System.out.println("Poziv " + function.getName());
	}

	public void constUsed(Obj o, int line) {
		System.out.println("Konstanta " + o.getName());
		
	}

	public void varUsed(Obj o, int line, boolean global) {
		String what;
		if (global)
			what = "Globalna promenljiva ";
		else
			what = "Lokalna promenljiva ";
		System.out.println(what + o.getName());		
	}
	
	public void fieldUsed(Obj o, int line)
	{
		System.out.println("Polje " + o.getName());
	}

	public void objectCreate(Obj obj, int line) {
		System.out.println("Kreiran objekat " + obj.getName());
		
	}
}
